
// require fs - node's build in filesystem API
// see the docs https://nodejs.org/api/fs.html
const fs = require('fs');

/**
* File module
*/
const file = function() {
  'use strict'; //strict mode has various improvements to JS

  //our module - an object
  let file = {};

  //the file we're going to read
  const FILENAME = "./files/demo.txt";

  /**
  * the read Method
  * @param {String} fileName
  * @returns Promise.then(Buffer).catch(err)
  */
  function read(fileName) {

    logToDom(`reading file ${fileName}`)

    //We're using promises to handle callback functions - see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
    //a promise represents an incomplete async operation - like reading a file or querying the db
    //a Promise takes a fuction as an argument - this function gets the resolve & reject methods as arguments
    //where resolve returns a successfull operation you can handle with .then()
    //and reject rejects the promise - there was an error, these are handled with .catch() -> change the filename to something else to see the error
    return new Promise(function(resolve, reject) {

      //We're using fs.readFile(filename, callbackFunction)
      //One of node's patterns is callback function where the first argument is always an error - if it's undefined we can continue
      //note the ES6 arrow function here - funtion(err, file){} is the same as (err, file)=>{} basically a shorthand
      fs.readFile(fileName, (err, fileBuffer) => {
          if(err) reject(err);
          else resolve(fileBuffer);
      });
      //note that fs also has a synchronuous method fs.readFileSync(file[, options])
    });
  }

  /**
  * Log a message to the DOM
  * @param {String} message
  */
  function logToDom(message) {

      //Vanilla Frontend stuff
      //Don't have to do this when using a framework like Vue - just assing the data to the model and the framework updates the DOM
      let element = document.createElement('p');
      element.textContent = message;
      document.body.appendChild(element);

  }

  /**
  * Buffer to String
  * @param {Buffer} buf
  * @returns {String}
  */
  function getFileFromBuffer(buf) {
    return buf.toString();
  }

  /**
  * Public read method
  */
  file.read = function () {

      read(FILENAME)
      .then((fileBuffer)=>{
        console.log("Buffer",fileBuffer);
        logToDom("file read successfully, file:");
        let fileRes =  getFileFromBuffer(fileBuffer);
        logToDom(fileRes);
      })
      .catch((err) => {console.error(err);})
  };

  //call the read method for demo purposes
  file.read();

  //return the file object
  return file;
};


//By using require you have to specify what you're exporting
//in this case we're exporting a module or a factory - a function that returns an object - similar to a class but not quite
module.exports = file();
